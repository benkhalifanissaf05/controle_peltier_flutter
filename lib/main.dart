import 'package:controlepeltier/authe/forget.dart';
import 'package:controlepeltier/authe/login.dart';
import 'package:controlepeltier/authe/signUp.dart';
import 'package:controlepeltier/authe/slide/intro_screen_custom_config.dart';
import 'package:controlepeltier/authe/slide/intro_screen_custom_layout.dart';
import 'package:controlepeltier/authe/slide/intro_screen_default.dart';
import 'package:controlepeltier/authe/verifyEmail.dart';
import 'package:controlepeltier/homeWidget/factures.dart';
import 'package:flutter/material.dart';
import 'package:controlepeltier/splashScreen/splashScrenn.dart';

void main() {
  runApp(myApp());
}

class myApp extends StatelessWidget {
    static const type = 1;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, 
       home: type == 0
             // ?Factures()
         ? IntroScreenDefault()
          : type == 1
              ? IntroScreenCustomConfig()
              : type == 2
                  ? IntroScreenCustomLayout()
                  : IntroScreenDefault(),
                 
                  //: SplashPage(),
   //   initialRoute: '/',
 routes: {
      
    '/splash': (context) => SplashPage(),
   // '/': (context)=> VerifyEmail(),
    "/login": (context) => Login(),
      //'/':(context) => SingUp()
      //"/forgot":(context)=>ForgotPasword()
    },
    
    //home: Text('hello nasssoufa '),*/
    );
  }
}

import 'package:flutter/material.dart';

class Factures extends StatefulWidget {
  const Factures({Key? key}) : super(key: key);

  @override
  _FacturesState createState() => _FacturesState();
}

class _FacturesState extends State<Factures> {
  double sizeIndicator = 45;
  @override
  Widget build(BuildContext context) {
   return MaterialApp(
    debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
      appBar: AppBar(
       // leadingWidth: 100,
       toolbarHeight: 100,
        centerTitle: true,
        leading: Icon(Icons.settings),
        title: const Text('Factures', style: TextStyle(fontWeight: FontWeight.bold),),
        backgroundColor: Color.fromARGB(255, 107, 18, 231),
      actions: <Widget>[

       IconButton(onPressed: (){

       }, icon: const Icon(Icons.search_outlined)),
       
        ],
        bottom: const TabBar(
              tabs: [
                Tab(
                text: 'TOUTES',),
                Tab( text: 'NON PAYÉES',),
                Tab( text: 'PAYÉES',),
              ],
            ),
      ),
      
      body: 
           Center(),
            bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 70.0,
       child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
     children:<Widget> [
              Column(
                children:<Widget> [  Image(
                        image: AssetImage("images/billIcon.png",
                        
                    ),
              height: sizeIndicator,
             // width: 45,
                      ),
                      Text('Facture')]
              ),
             
        SizedBox(),
      Column(
                children:<Widget> [  Image(
                        image: AssetImage("images/devis.png",
                        
                    ),
              height: 45,
                      ),
                      Text('Devis')]
              ),
               Column(
                children:<Widget> [
              
                  
                    Image(
                        image: AssetImage("images/client.png",
                     
                    ),
                    
              height: 45,
                      ),
                       
                      Text('Client')]
              ),
             
        SizedBox(),
      Column(
                children:<Widget> [  Image(
                        image: AssetImage("images/article.png",
                        
                    ),
              height: 45,
                      ),
                      Text('Articles')]
              ),
                SizedBox(),
      Column(
                children:<Widget> [  Image(
                        image: AssetImage("images/rapport.png",
                        
                    ),
              height: 45,
                      ),
                      Text('Rapport')]
              ),
     ]
       ),
       ),
      ),
             
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color.fromARGB(255, 107, 18, 231),
        onPressed: () => setState(() => 
        print('count++')),
        
        child: const Icon(Icons.add),),
    //floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      )));
  }
}

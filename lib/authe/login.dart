import 'package:controlepeltier/authe/forget.dart';
import 'package:controlepeltier/authe/signUp.dart';
import 'package:controlepeltier/homeWidget/factures.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:simple_icons/simple_icons.dart';
import 'package:controlepeltier/constant/color.dart';

import 'package:flutter/material.dart';


class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      
        body: SingleChildScrollView(
            padding: EdgeInsets.only(left: 7.0, right: 7.0, top: 90.0),
            
            child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                // alignment: Alignment.center, 
                  child: const Text(
                    'ITGate Training',
                    style: TextStyle(
                        fontSize: 30,
                        fontStyle: FontStyle.normal,
                        color: Green),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child:  const 
                      Image(
                        image: AssetImage("images/image1.png"),
                        height: 180.0,
                        width: 180,
                      ),
                ),
                Container(
                    width: 320,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: email,
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Enter Your Email Here...',
                        prefixIcon: Icon(Icons.email),
                        suffixIcon: Align(
                          widthFactor: 1.0,
                          heightFactor: 1.0,
                          child: Icon(
                            // Icons.remove_red_eye,
                            Icons.verified_rounded, color: Green,
                          ),
                        ),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: white,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    )),
                Container(
                    width: 320,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: password,
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Enter Your Password Here...',
                        prefixIcon: Icon(Icons.key),
                        suffixIcon: Align(
                          widthFactor: 1.0,
                          heightFactor: 1.0,
                          child: Icon(
                            Icons.remove_red_eye,
                            color: Green,
                          ),
                        ),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: white,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    )),
                Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                  
                  children: <Widget>[
               Container(child: Icon(Icons.verified_user_sharp, color: Green,),
               
                alignment: Alignment.bottomLeft, margin: const EdgeInsets.only(right: 9),) ,
             Container(child :const Text('Remember Me', style: TextStyle(fontSize: 15, color: gry, ),
              ),
               margin: const EdgeInsets.only(right: 50)
 ),
                   const SizedBox(height: 30),
                    TextButton(
                    style: ButtonStyle (alignment: Alignment.centerRight, ),
                      child: const Text(
                        'Forgot Passowrd?',
                        style: TextStyle(fontSize: 15, color: gry),
                        
                      ),
                      onPressed: () 
                        async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const ForgotPasword()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                        //signup forget 
                      },
                    
                    ),
              
                  ],
                
                
                  
                ),
                Container(
                height: 60,
                  width: 320,
                  padding: EdgeInsets.all(10.0),
                  //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                 
                child: ElevatedButton(
                  child: const Text('Login', style: TextStyle(fontSize: 20),
                  ),
                  style: ButtonStyle(
                    
                    ),
                   onPressed: () 
                        async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Factures()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                        //signup forget 
                      },
                )
            ),
Container(
                height: 60,
                  width: 320,
                  padding: EdgeInsets.all(10.0),
                  //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                 
                child:   ElevatedButton(
                  style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white // Background color
  ),
                  child: const Text('Creat A New Account', style: TextStyle(fontSize: 15,  color: gry ),
                ),
                 
                   onPressed: () 
                        async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const SingUp()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                        //signup forget 
                      },
                )
            ),
             const SizedBox(height: 10),
          Container(
            child: Text('Or via social media',)
          ),  const SizedBox(height: 10),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 
                children: <Widget>[
                 Icon(
              SimpleIcons.gmail,
              color: Colors.red,
               size: 30,
               
            
            ), const SizedBox(width: 20),
              Icon(
              SimpleIcons.facebook,
              color: Color.fromARGB(255, 9, 130, 229),
              size: 30,
            ),
             const SizedBox(width: 20),
              Icon(
              SimpleIcons.linkedin,
              //color: Colors.red,
              size: 30,
            
            ),

        /*     Icon(
              EvaIcons.linkedin,
              //color: Colors.red,
            
            ),*/
                ],
               )
              ],
            )));
       

    /*const Card(
      child: Padding(
        padding: EdgeInsets.all(90),
        child: Text('Hello World!'),
       
      ),
    );*/
  }
}

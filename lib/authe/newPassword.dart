import 'package:controlepeltier/authe/NewPassword.dart';
import 'package:controlepeltier/constant/color.dart';
import 'package:flutter/material.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({Key? key}) : super(key: key);
  @override
  State<NewPassword> createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordControlle = TextEditingController();

  //String initialCountry = 'NG';
  // PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          leadingWidth: 70,
          toolbarHeight: 70,
          elevation: 0,
          title: const Text(
            'Forgot Password',
            style: TextStyle(color: Colors.black, fontSize: 30),
          ),
          backgroundColor: Color.fromARGB(255, 246, 246, 246),
          leading: IconButton(
            //  color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            ),
            //replace with our own icon data.
          ),
          centerTitle: true,
        ),
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(children: <Widget>[
              Container(
                child: const Image(
                  image: AssetImage("images/lock1.png"),
                  height: 180.0,
                  width: 180,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Center(
                child: Container(
                    margin: const EdgeInsets.all(5.0),
                    width: 330,
                    child: Wrap(runAlignment: WrapAlignment.center, children: [
                      Flexible(
                        child: new Text(
                          "Your Password Must Be Diffrent From Perviously Used Password. ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              height: 2.5,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ])),
              ),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: passwordControlle,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'New Password',
                      prefixIcon: Icon(Icons.lock_outline),
                      suffixIcon: Align(
                        widthFactor: 1.0,
                        heightFactor: 1.0,
                        child: Icon(
                          Icons.remove_red_eye_outlined,
                        ),
                      ),
                      hintStyle: TextStyle(color: Colors.grey),
                      //filled: true,
                      fillColor: white,
                    ),
                  )),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: passwordControlle,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'Confirme Password',
                      prefixIcon: Icon(Icons.lock_outline),
                      suffixIcon: Align(
                        widthFactor: 1.0,
                        heightFactor: 1.0,
                        child: Icon(
                          Icons.remove_red_eye_outlined,
                        ),
                      ),
                      hintStyle: TextStyle(color: Colors.grey),
                      //filled: true,
                      fillColor: white,
                    ),
                  )),
              SizedBox(
                height: 15,
              ),
              Align(
                  alignment: Alignment.center,
                  child: Wrap(
                    runAlignment: WrapAlignment.center,
                    children: [
                      GestureDetector(
                        child: const Text(
                          'Change Password',
                          style: TextStyle(
                              color: Color.fromARGB(255, 6, 132, 149),
                              decoration: TextDecoration.underline),
                        ),
                        onTap: () async {
                          try {
                            print("cc");
                            /* Navigator.push(
                        //context,
                        //MaterialPageRoute(builder: (context) => const NewPassword()),
                      );*/
                            // Navigator:
                            // (context) => Login();
                          } catch (err) {
                            debugPrint('Something bad happened');
                          }
                          //signup forget
                        },
                      ),
                    ],
                  )),
              const SizedBox(height: 10),
              Container(
                  height: 60,
                  width: 320,
                  padding: EdgeInsets.all(10.0),
                  //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),

                  child: ElevatedButton(
                    child: const Text(
                      'Save',
                      style: TextStyle(fontSize: 20),
                    ),
                    style: ButtonStyle(),
                    onPressed: () {
                      // home page
                    },
                  )),
            ])));
  }
}

//

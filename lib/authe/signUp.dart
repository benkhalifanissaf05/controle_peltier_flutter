import 'package:controlepeltier/authe/login.dart';
import 'package:controlepeltier/constant/chekBox.dart';
import 'package:controlepeltier/constant/color.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class SingUp extends StatefulWidget {
  const SingUp({Key? key}) : super(key: key);
  @override
  State<SingUp> createState() => _SingUpState();
}

class _SingUpState extends State<SingUp> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordControlle = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  //String initialCountry = 'NG';
  // PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext) {
    return Scaffold(
      appBar: AppBar(),
        body: Center(
            child: Column(

                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              child: const Text(
                'Hey Here',
                style: TextStyle(
                    fontSize: 30, fontStyle: FontStyle.normal, color: black),
              )),
          Container(
            padding: EdgeInsets.all(10),
            child: Text(
              'Creat An Account ',
              style: TextStyle(
                  fontSize: 30, fontWeight: FontWeight.bold, color: black),
            ),
          ),
          Row(
              //mainAxisAlignment: MainAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 180,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: firstNameController,
                    decoration: InputDecoration(
                        hintText: ('First Name '),
                        prefixIcon: Icon(Icons.person)),
                  ),
                ),
                Container(
                  width: 180,
                  padding: EdgeInsets.all(12.0),
                  child: TextField(
                    controller: lastNameController,
                    decoration: InputDecoration(
                        hintText: ('Last Name '),
                        prefixIcon: Icon(Icons.person)),
                  ),
                ),
              ]),
          Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              controller: userNameController,
              decoration: InputDecoration(
                  hintText: ('User Name '),
                  prefixIcon: Icon(Icons.person_2_outlined)),
            ),
          ),
          Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                  controller: emailController,
                  autocorrect: true,
                  decoration: InputDecoration(
                    hintText: 'Test@gmail.com',
                    prefixIcon: Icon(Icons.email_outlined),
                    suffixIcon: Align(
                      widthFactor: 1.0,
                      heightFactor: 1.0,
                      child: Icon(
                        // Icons.remove_red_eye,
                        Icons.verified_outlined,
                      ),
                    ),
                    hintStyle: TextStyle(color: Colors.grey),
                    filled: true,
                    fillColor: white,
                  ))),
          Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                controller: passwordControlle,
                autocorrect: true,
                decoration: InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  suffixIcon: Align(
                    widthFactor: 1.0,
                    heightFactor: 1.0,
                    child: Icon(
                      Icons.remove_red_eye_outlined,
                    ),
                  ),
                  hintStyle: TextStyle(color: Colors.grey),
                  //filled: true,
                  fillColor: white,
                ),
              )),
          Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                controller: passwordControlle,
                autocorrect: true,
                decoration: InputDecoration(
                  hintText: 'Confirme Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  suffixIcon: Align(
                    widthFactor: 1.0,
                    heightFactor: 1.0,
                    child: Icon(
                      Icons.remove_red_eye_outlined,
                    ),
                  ),
                  hintStyle: TextStyle(color: Colors.grey),
                  //filled: true,
                  fillColor: white,
                ),
              )),
          Stack(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 501,
                    padding: EdgeInsets.all(10.0),
                    child: Form(
                      key: formKey,
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InternationalPhoneNumberInput(
                              onInputChanged: (PhoneNumber number) {
                                print(number.phoneNumber);
                              },
                              onInputValidated: (bool value) {
                                print(value);
                              },
                              //hintText: '',

                              selectorConfig: SelectorConfig(
                                selectorType: PhoneInputSelectorType
                                    .BOTTOM_SHEET, // type des drapeaux
                              ),

                              // ignoreBlank: false,
                              //autoValidateMode: AutovalidateMode.disabled,
                              //selectorTextStyle: TextStyle(color: Colors.black),
                              //initialValue: number,
                              textFieldController: mobileController,
                              //formatInput: true,
                              /* keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),*/
                              //   inputBorder: OutlineInputBorder(),
                              /* onSaved: (PhoneNumber number) {
                print('On Saved: $number');
              },*/
                            ),
                          ],
                        ),
                      ),
                    ))
              ]),
          Container(
              //  padding: EdgeInsets.all(10.0)
              child: Row(
            children: <Widget>[
              chekBox(),
              const SizedBox(height: 10),
              Flexible(
                  child: Align(
                alignment: Alignment.center,
                child: Wrap(runAlignment: WrapAlignment.center, children: [
                  const Text('by creating an account you agree to '),
                  GestureDetector(
                      child: const Text(
                        'our Conditions of Use ',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                      onTap: () async {
                        try {
                          print("qlque chose");
                        } catch (err) {
                          debugPrint('Something bad happened');
                        }
                      }),
                  const Text(
                    'And  ',
                  ),
                  GestureDetector(
                      child: Text(
                        'Privacy Notice',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                      onTap: () async {
                        try {
                          print("qlque chose");
                        } catch (err) {
                          debugPrint('Something bad happened');
                        }
                      })
                ]),
              ))
            ],
          )),
          const SizedBox(height: 10),
          Container(
              height: 60,
              width: 320,
              padding: EdgeInsets.all(10.0),
              //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),

              child: ElevatedButton(
                child: const Text(
                  'Registre',
                  style: TextStyle(fontSize: 20),
                ),
                style: ButtonStyle(),
                onPressed: () {
                  // home page
                },
              )),
          Container(
            child: Wrap(runAlignment: WrapAlignment.center, children: [
              const Text('Alerady have an Account  '),
              GestureDetector(
                  child: const Text(
                    'Login ',
                    style: TextStyle(
                        color: Green, decoration: TextDecoration.underline),
                  ),
                  onTap: () async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Login()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                  })
            ]),
          )
        ])));
  }
}

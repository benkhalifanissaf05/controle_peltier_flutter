import 'package:controlepeltier/authe/OtpInput.dart';
import 'package:controlepeltier/constant/color.dart';
import 'package:flutter/material.dart';
import 'package:controlepeltier/authe/newPassword.dart';

class VerifyEmail extends StatefulWidget {
  const VerifyEmail({Key? key}) : super(key: key);
  @override
  State<VerifyEmail> createState() => _VerifyEmailState();
}

class _VerifyEmailState extends State<VerifyEmail> {
  TextEditingController firstnumberController = TextEditingController();
  TextEditingController secondnumberController = TextEditingController();
  TextEditingController thridnumberController = TextEditingController();
  TextEditingController lastnumberController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordControlle = TextEditingController();

  //String initialCountry = 'NG';
  // PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          leadingWidth: 70,
          toolbarHeight: 70,
          elevation: 0,
          title: const Text(
            'Verify Your Email',
            style: TextStyle(color: Colors.black, fontSize: 30),
          ),
          backgroundColor: Color.fromARGB(255, 246, 246, 246),
          leading: IconButton(
            //  color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            ),
            //replace with our own icon data.
          ),
          centerTitle: true,
        ),
        body: Padding(padding: const EdgeInsets.all(10), 
        child: ListView(
        
          children: <Widget>[
            Container(
        
              child: 
              const 
                      Image(
                        image: AssetImage("images/email.png"),
                        height: 180.0,
                        width: 180,
                      ),
            ),
            SizedBox(height: 30 ,),
          Center(
            child: Container(
               margin: const EdgeInsets.all( 5.0),
              width: 330,
          child: Wrap(
            runAlignment: WrapAlignment.center,
            
             children: [
                Flexible(
                  child:new Text ("Please Enter Your the 4 Digit Code To  ", textAlign: TextAlign.center, style: TextStyle(height:2.5, fontSize: 18, fontWeight: FontWeight.bold),),
                   
                   ) ])
            ),
          
          )  ,
         
            Center(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            
            children: [
            
              OtpInput(firstnumberController, true), 
       
              OtpInput(secondnumberController, false),
              OtpInput(thridnumberController, false),
              OtpInput(lastnumberController, false)
            ],
          ),
            ) ,
       
                  SizedBox(height: 15,),
                  Align(
                alignment: Alignment.center,
                child: Wrap(runAlignment: WrapAlignment.center, children: [
                 
                  GestureDetector(
                      child: const Text(
                        'Resend Code',
                        style: TextStyle(
                            color:  Color.fromARGB(255, 6, 132, 149) ,
                            decoration: TextDecoration.underline),
                      ),
                      onTap: () async {
                        try {
                          print("qlque chose");
                        } catch (err) {
                          debugPrint('Something bad happened');
                        }
                      }),
          ],
        )),
         const SizedBox(height: 10),
          Container(
              height: 60,
              width: 320,
              padding: EdgeInsets.all(10.0),
              //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),

              child: ElevatedButton(
                child: const Text(
                  'Verify',
                  style: TextStyle(fontSize: 20),
                ),
                style: ButtonStyle(),
                onPressed:  () async {
                          try {
                           
                            Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const NewPassword ()),
                      );
                            // Navigator:
                            // (context) => Login();
                          } catch (err) {
                            debugPrint('Something bad happened');
                          }
                          //signup forget
                        },
              )),
                 
          ]
              )
        ));
  }
}
  
//
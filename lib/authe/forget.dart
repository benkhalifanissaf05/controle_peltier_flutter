import 'package:controlepeltier/authe/verifyEmail.dart';
import 'package:controlepeltier/constant/color.dart';
import 'package:flutter/material.dart';

class ForgotPasword extends StatefulWidget {
  const ForgotPasword({Key? key}) : super(key: key);
  @override
  State<ForgotPasword> createState() => _ForgotPaswordState();
}

class _ForgotPaswordState extends State<ForgotPasword> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordControlle = TextEditingController();

  //String initialCountry = 'NG';
  // PhoneNumber number = PhoneNumber(isoCode: 'NG');

  @override
  Widget build(BuildContext) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          leadingWidth: 70,
          toolbarHeight: 70,
          elevation: 0,
          title: const Text(
            'Forgot Password',
            style: TextStyle(color: Colors.black, fontSize: 30),
          ),
          backgroundColor: Color.fromARGB(255, 246, 246, 246),
          leading: IconButton(
            //  color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Color.fromARGB(255, 7, 7, 7), // <-- SEE HERE
            ),
            //replace with our own icon data.
          ),
          centerTitle: true,
        ),
        body: Padding(padding: const EdgeInsets.all(10), 
        child: ListView(
        
          children: <Widget>[
            Container(
        
              child: 
              const 
                      Image(
                        image: AssetImage("images/lock.png"),
                        height: 180.0,
                        width: 180,
                      ),
            ),
            SizedBox(height: 30 ,),
          Center(
            child: Container(
               margin: const EdgeInsets.all( 5.0),
              width: 330,
          child: Wrap(
            runAlignment: WrapAlignment.center,
            
             children: [
                Flexible(
                  child:new Text ("Please Enter Your Email Adresse To Recive Your a Verification Code. ", textAlign: TextAlign.center, style: TextStyle(height:2.5, fontSize: 18, fontWeight: FontWeight.bold),),
                   
                   ) ])
            ),
          
          )  ,
           Container(
              padding: EdgeInsets.all(10.0),
              child: TextField(
                  controller: emailController,
                  autocorrect: true,
                  decoration: InputDecoration(
                    hintText: 'Test@gmail.com',
                    prefixIcon: Icon(Icons.email_outlined),
                    suffixIcon: Align(
                      widthFactor: 1.0,
                      heightFactor: 1.0,
                      child: Icon(
                        // Icons.remove_red_eye,
                        Icons.verified_outlined,
                      ),
                    ),
                    hintStyle: TextStyle(color: Colors.grey),
                    filled: true,
                    fillColor: white,
                  ))),
                  SizedBox(height: 15,),
                  Align(
                alignment: Alignment.center,
                child: Wrap(runAlignment: WrapAlignment.center, children: [
                 
                  GestureDetector(
                      child: const Text(
                        'Try another way',
                        style: TextStyle(
                            color:  Color.fromARGB(255, 6, 132, 149) ,
                            decoration: TextDecoration.underline),
                      ),
                      onTap: ()  async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const VerifyEmail()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                        //signup forget 
                      },),
          ],
        )),
         const SizedBox(height: 10),
          Container(
              height: 60,
              width: 320,
              padding: EdgeInsets.all(10.0),
              //padding: EdgeInsets.fromLTRB(0, 10, 0, 10),

              child: ElevatedButton(
                child: const Text(
                  'Send',
                  style: TextStyle(fontSize: 20),
                ),
                style: ButtonStyle(),
                onPressed:  ()  async {
                    try {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const VerifyEmail()),
                      );
                      // Navigator:
                      // (context) => Login();
                    } catch (err) {
                      debugPrint('Something bad happened');
                    }
                        //signup forget 
                      },
              )),])
        ));
  }
}
  
//
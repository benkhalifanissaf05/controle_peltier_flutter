
import 'package:flutter/material.dart';
import 'package:easy_splash_screen/easy_splash_screen.dart';
class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return EasySplashScreen(
     
     logo : Image.asset('images/itgate.png',),  
     
   /*
      title: Text(
        "TFD",
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
         
        ),
      ),*/
      backgroundColor: Color.fromARGB(246, 255, 253, 252),
      showLoader: true,
      
      loadingText: Text("Loading..." , style: TextStyle( fontStyle: FontStyle.italic)),
      durationInSeconds: 3,
      navigator: "/login",
     
    );
  }
}